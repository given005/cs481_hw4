﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace cs481_hw4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
  
        public static ObservableCollection<genre> Gametypes;
        public class genre
        {
            public string Title { get; set; }
           
            public string Img { get; set; }
           
          
        }
        public MainPage()
        {
            InitializeComponent();
            Gametypes = new ObservableCollection<genre>() {
                new genre()
                {
                    Title="Shooter",
                    Img="shooter.jpg",                    
                },
                new genre()
                {
                    Title="Fighting",
                    Img="fighting.jpg",
                },
                new genre()
                {
                    Title="Puzzle",
                    Img="puzzle.jpg",
                },
                new genre()
                {
                    Title="RPG",
                    Img="rpg.jpg",
                },
                new genre()
                {
                    Title="Rougelike",
                    Img="rougelike.jpg",
                },
                new genre()
                {
                    Title="Platformer",
                    Img="platform.jpg",
                },
                new genre()
                {
                    Title="Horror",
                    Img="horror.jpg",
                },
            };

            BindingContext = this;
            games.IsPullToRefreshEnabled = true;
            games.Refreshing += Games_Refreshing;
            games.ItemsSource = Gametypes;
            
        }

        
        private void Games_Refreshing(object sender, EventArgs e)
        {
            var list = (ListView)sender;
            var itemlst = Gametypes.Reverse().ToList();
            Gametypes.Clear();
            foreach(var s in itemlst)
            {
                Gametypes.Add(s);
            }
            list.IsRefreshing = false;
        }

        private void Games_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            genre genre = e.SelectedItem as genre;
            if (genre.Title == "Shooter")
            {
                Navigation.PushAsync(new shooter());
            }
            else if(genre.Title == "Fighting")
            {
                Navigation.PushAsync(new Fighting());
            }
            else if (genre.Title == "Puzzle")
            {
                Navigation.PushAsync(new Puzzle());
            }
            else if (genre.Title == "RPG")
            {
                Navigation.PushAsync(new RPG());
            }
            else if (genre.Title == "Rougelike")
            {
                Navigation.PushAsync(new Rougelike());
            }
            else if (genre.Title == "Platformer")
            {
                Navigation.PushAsync(new Platformer());
            }
            else if (genre.Title == "Horror")
            {
                Navigation.PushAsync(new Horror());
            }
        }

        
        public void OnMore(object sender, EventArgs e)
        {
            
            var mi = ((MenuItem)sender);
            DisplayAlert("Personal Favorites", "Shooter: Borderlands\nHorror: Resident Evil 4\nFighting: Soul Calibur\nPuzzle: Proffesor Layton\nRPG: Dragon Quest XI\nPlatformer: Banjo-Kazooie\nRougeLike: Dead Cells", "OK");
        }
        
        void Handle_Refreshing(System.Object sender, System.EventArgs e)
        {
            games.IsRefreshing = false;
        }


        private void games_ItemSelected_1(object sender, SelectedItemChangedEventArgs e)
        {
            
        }
    }
}
